# MyLocation
Application to search your location and send coordinates to some applications
          
## Example

![alt text](https://gitlab.com/Airatonline/MyLocation/raw/images/first.png?inline=false)

## Download

[![](https://gitlab.com/Airatonline/MyLocation/raw/e560b16b50efdd6b4764b386ddd2ffec3bbbefd1/.myLocation/google-play-badge.png)](https://play.google.com/store/apps/details?id=com.Air.airatonline.mylocation)