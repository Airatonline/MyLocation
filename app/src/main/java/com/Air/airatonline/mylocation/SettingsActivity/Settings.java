package com.Air.airatonline.mylocation;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class Settings extends AppCompatActivity {

    EditText text;
    TextView helpText, resultText;
    CheckBox google_chb, yandex_chb;
    SharedPreferences mEdit;
    String SEND_TEXT;
    boolean GOOGLE_CHECKED, YANDEX_CHECKED;
    String result = "";
    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.textAction();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        google_chb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.textAction();
            }
        });
        yandex_chb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.textAction();
            }
        });


    }
    void init(){
        mEdit = getSharedPreferences("Settings", MODE_PRIVATE);
        presenter = new Presenter(this);
        //инициализация

        text = findViewById(R.id.text);
        helpText = findViewById(R.id.textView);
        resultText = findViewById(R.id.textView2);
        google_chb = findViewById(R.id.checkBox);
        yandex_chb = findViewById(R.id.checkBox2);

        text.setText(mEdit.getString("SEND_TEXT", ""));
        resultText.setText((mEdit.getString("SEND_TEXT", "").replace("$geo", "(" + getString(R.string.your_location) + ")")));
        google_chb.setChecked(mEdit.getBoolean("GOOGLE_CHECKED", false));
        yandex_chb.setChecked(mEdit.getBoolean("YANDEX_CHECKED", false));
    }



    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = mEdit.edit();
        editor.putString("SEND_TEXT", text.getText().toString());
        editor.putBoolean("GOOGLE_CHECKED", GOOGLE_CHECKED);
        editor.putBoolean("YANDEX_CHECKED", YANDEX_CHECKED);
        editor.apply();
        editor.commit();
    }
}
