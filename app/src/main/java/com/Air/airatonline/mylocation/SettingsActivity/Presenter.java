package com.Air.airatonline.mylocation;

public class SettingsActivity{

    Settings activity;

    SettingsActivity(Settings activity){
    this.activity = activity;
    }

    private void textAction() {
        result = (text.getText().toString()).replace("$geo", "(" + getString(R.string.your_location) + ")");


        if (google_chb.isChecked()) {
            result += "\n" + getString(R.string.google_url);
            GOOGLE_CHECKED = true;
        } else {
            GOOGLE_CHECKED = false;
        }
        if (yandex_chb.isChecked()) {
            result += "\n" + getString(R.string.yandex_url);
            YANDEX_CHECKED = true;
        } else {
            YANDEX_CHECKED = false;
        }

        resultText.setText(result);
        SEND_TEXT = result;
    }
}