package com.Air.airatonline.mylocation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


   public ImageButton send; //кнопка отправки
   public ImageButton btn; // кнопка местоположения
   public ImageButton startSettings; // кнопка настроек
   public View mapCheck;

    private GoogleMap mMap;
    private Context context = this;
    Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        init();

        presenter = new Presenter(this);


        mapCheck.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                presenter.mapCheckTouchListener(v, event)
            }
        });


        //кнопка определения местоположения
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.myLocationBTNListener()
            }
        });

        //кнопка открытия настроек
        startSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startSettingsActivity()
            }
        });

        //кнопка отправки местоположения
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendLocationInfo();
            }
        });

        //фрагмент для работы с картами



        //работа с GPS



        startLocated();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        presenter.locationManager.requestLocationUpdates("gps", 500, 0, presenter.locationListener);
    }

    protected void init() {
        send = findViewById(R.id.btn_send);
        btn = findViewById(R.id.btn_location);
        startSettings = findViewById(R.id.floatingActionButton2);
        mapCheck = findViewById(R.id.mapCheck);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        send.setImageResource(R.drawable.ic_share_black_24dp);
        send.setBackgroundColor(Color.TRANSPARENT);

        startSettings.setImageResource(R.drawable.ic_dehaze_black_24dp);
        startSettings.setBackgroundColor(Color.TRANSPARENT);

        btn.setImageResource(R.drawable.ic_my_location_black_24dp);
        btn.setBackgroundColor(Color.TRANSPARENT);
    }

    protected void startLocated() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,

                }, 10);
                return;
            }
        }
        presenter.locationManager.requestLocationUpdates("gps", 500, 0, presenter.ocationListener);
    }

}
