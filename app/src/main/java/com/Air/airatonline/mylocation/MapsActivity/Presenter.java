package com.Air.airatonline.mylocation;

public class Presenter{

    MapsActivity activity;
    Context context;
    public boolean follow = true;
    public LocationManager locationManager;
    public LocationListener locationListener;
    public LatLng loc;


    Presenter(MapsActivity activity, Context context){
        this.activity = activity;
        this.context = context;

        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (mMap != null) {
                    mMap.clear();
                    loc = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(loc));
                    mMap.addCircle(new CircleOptions().center(loc).
                            radius(location.getAccuracy()).
                            fillColor(Color.argb(50, 0, 0, 255)).
                            strokeColor(Color.argb(150, 0, 0, 255)));
                    if (follow) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(20), 500, null);
                    }
                }


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                if (ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates("gps", 500, 0, locationListener);

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
    }

    void mapCheckTouchListener(View v, MoutionEvent event){
        v.performClick();
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            activity.follow = false;
        }
        return false;
    }


    void myLocationBTNListener(View v){
        v.performClick();
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            follow = false;
        }
        return false;
    }
    void startSettingsActivity(){
        Intent intent = new Intent(context, settings.class);
        context.startActivity(intent);
    }
    void sendLocationInfo(){
        if (activity.loc != null) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            //сюда нужно добавить инфу из Shared preference
            SharedPreferences mPref = getSharedPreferences("Settings", MODE_PRIVATE);
            String send_text = mPref.getString("SEND_TEXT", "");
            Log.d("send_text", send_text);
            send_text = send_text.replace("$geo", ("\n" + (loc.latitude) + "\n" + (loc.longitude)));
            if (mPref.getBoolean("GOOGLE_CHECKED", false)) {
                send_text += "\nGoogle maps:" + "https://google.com/maps/search/?api=1&query=" + loc.latitude + "," + loc.longitude;
            }
            if (mPref.getBoolean("YANDEX_CHECKED", false)) {
                send_text += "\nYandex maps: http://yandex.com/maps/?text=" + loc.latitude + "," + loc.longitude;
            }
            Log.d("send_text", send_text);

            intent.putExtra(Intent.EXTRA_TEXT, send_text);
            intent.setType("text/plain");
            context.startActivity(intent);
        } else {
            Snackbar.make(v, "Ваше местоположение не найдено", Snackbar.LENGTH_LONG).show();
        }
    }

}